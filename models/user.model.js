'use strict';
var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
	username: String,
	password: String,
	creationDate: Date
});

const User = mongoose.model('User', UserSchema);

module.exports = User;