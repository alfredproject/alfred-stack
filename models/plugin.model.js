'use strict';
var mongoose = require('mongoose');

var PluginSchema = new mongoose.Schema({
	name: String,
	author: String,
	version: String
});

const Plugin = mongoose.model('Plugin', PluginSchema);

module.exports = Plugin;