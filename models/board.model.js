'use strict';
var mongoose = require('mongoose');

var BoardSchema = new mongoose.Schema({
	name: String,
	description: String,
	plugin: {type: mongoose.Schema.Types.ObjectId, ref: 'Plugin'},
	active: Boolean
});

const Board = mongoose.model('Board', BoardSchema);

module.exports = Board;