'use strict';
const Board = require('../models/board.model');

const _this = this;

exports.getBoards = async function(query) {
	try {
		var boards = await Board.find(query);
		return boards;
	}  catch(e) {
        throw Error('Error while querying Boards');
	}
};

exports.getBoardById = async function(id) {
	try {
		var board = await Board.findById(id);
		return board;
	} catch(e) {
		throw Error('Error while querying Board by ID');
	}
};

exports.createBoard = async function(board) {
	var newBoard = new Board({
		name: board.name,
		description: board.description,
		plugin: board.plugin,
		active: false
	});

	try {
		var savedBoard = await newBoard.save();
		return savedBoard;
	} catch(e) {
		throw Error('Error while saving new Board');
	}
};

exports.updateBoard = async function(board) {
	var id = board.id;

	try {
		var toUpdateBoard = await Board.findById(id);
	} catch(e) {
		throw Error('Error while querying Board by ID');
	}

	if (!toUpdateBoard) return false;

	toUpdateBoard.name = board.name;
	toUpdateBoard.description = board.description;
	toUpdateBoard.plugin = board.plugin;
	toUpdateBoard.active = board.active;

	try {
		var updatedBoard = await toUpdateBoard.save();
		return updatedBoard;
	} catch(e) {
		throw Error('Error while updating Board');
	}
};

exports.deleteBoard = async function(id) {
	try {
		var deletedBoard = await Board.remove({_id: id});
		// if (deletedBoard.result.n === 0) throw Error('Board could not be deleted');
		return deletedBoard;
	} catch(e) {
		throw Error('Error while deleting Board');
	}
}