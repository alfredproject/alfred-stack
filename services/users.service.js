'use strict';
const User = require('../models/user.model');

const _this = this;

exports.getUsers = async function(query) {
	try {
		var users = await User.find(query);
		return users;
	}  catch(e) {
        throw Error('Error while querying Users');
	}
};

exports.getUserById = async function(id) {
	try {
		var user = await User.findById(id);
		return user;
	} catch(e) {
		throw Error('Error while querying User by ID');
	}
};

exports.createUser = async function(user) {
	var newUser = new User({
		username: user.username,
		password: user.password,
		creationDate: new Date()
	});

	try {
		var savedUser = await newUser.save();
		return savedUser;
	} catch(e) {
		throw Error('Error while saving new User');
	}
};

exports.updateUser = async function(user) {
	var id = user.id;

	try {
		var toUpdateUser = await User.findById(id);
	} catch(e) {
		throw Error('Error while querying User by ID');
	}

	if (!toUpdateUser) return false;

	toUpdateUser.username = user.username;
	toUpdateUser.password = user.password;

	try {
		var updatedUser = await toUpdateUser.save();
		return updatedUser;
	} catch(e) {
		throw Error('Error while updating User');
	}
};

exports.deleteUser = async function(id) {
	try {
		var deletedUser = await User.remove({'_id': id});
		//if (deletedUser.result.n === 0) throw Error('User could not be deleted');
		return deletedUser;
	} catch(e) {
		throw Error('Error while deleting User');
	}
}