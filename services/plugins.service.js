'use strict';
const Plugin = require('../models/plugin.model');

const _this = this;

exports.getPlugins = async function(query) {
	try {
		var plugins = await Plugin.find(query);
		return plugins
	} catch(e) {
		throw Error('Error while querying Plugins');
	}
};

exports.getPluginById = async function(id) {
	try {
		var plugin = await Plugin.findById(id);
		return plugin;
	} catch(e) {
		throw Error('Error while querying Plugin by ID');
	}
};

exports.createPlugin = async function(plugin) {
	var newPlugin = new Plugin({
		name: plugin.name,
		author: plugin.author,
		version: plugin.version
	});

	try {
		var savedPlugin = await newPlugin.save();
		return savedPlugin;
	} catch(e) {
		throw Error('Error while saving new Plugin');
	}
};

exports.updatePlugin = async function(plugin) {
	var id = plugin.id;

	try {
		var toUpdatePlugin = await Plugin.findById(id);
	} catch(e) {
		throw Error('Error while querying Plugin by ID');
	}

	if (!toUpdatePlugin) return false;

	toUpdatePlugin.name = plugin.name;
	toUpdatePlugin.author = plugin.author;
	toUpdatePlugin.version = plugin.version;

	try {
		var updatedPlugin = await toUpdatePlugin.save();
		return updatedPlugin;
	} catch(e) {
		throw Error('Error while updating Plugin');
	}
};

exports.deletePlugin = async function(id) {
	try {
		var deletedPlugin = await Plugin.remove({_id: id});
		if (deletedPlugin.result.n === 0) throw Error('Plugin could not be deleted');
		return deletedPlugin;
	} catch(e) {
		throw Error('Error while deleting Plugin');
	}
}