'use strict';
const mongoose = require('mongoose');
const PluginService = require('../services/plugins.service');
const sha1 = require('sha1');
const ExtensionLoader = require('./extension-loader.controller');

const _this = this;

exports.getPlugins = async function(req, res, next) {
	try {
		var plugins = await PluginService.getPlugins(req.body.query);
		var parsedPlugins = [];
		plugins.forEach(plugin => {
			plugin = plugin.toJSON();
			var extension = ExtensionLoader.extensions.find(function(ext) { return ext._id.toString() == plugin._id.toString(); });
			plugin['commands'] = extension.routes;
			parsedPlugins.push(plugin);
		});

		return res.status(200).json({status: 200, data: parsedPlugins});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.getPluginById = async function(req, res, next) {
	if(!req.params.id)
		return res.status(400).json({status: 400, message: "Id must be present"});

	var id = mongoose.mongo.ObjectId(req.params.id);

	try {
		var plugin = await PluginService.getPluginById(id);
		return res.status(200).json({status: 200, data: plugin});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.createPlugin = async function(req, res, next) {
	var plugin = {
		name: req.body.pluginData.name,
		author: req.body.pluginData.author,
		version: req.body.pluginData.version
	};

	try {
		var newPlugin = await PluginService.createPlugin(plugin);
		return res.status(200).json({status: 200, data: plugin});
	} catch(e) {
		return res.status(400).json({status: 400, message: "Unable to create Plugin"});
	}
};

exports.updatePlugin = async function(req, res, next) {
	if(!req.body.pluginData._id)
		return res.status(400).json({status: 400, message: "Id must be present"});

	var id = req.body.pluginData._id;

	var plugin = {
		id,
		name: req.body.pluginData.name ? req.body.pluginData.name : null,
		author: req.body.pluginData.author ? req.body.pluginData.author : null,
		version: req.body.pluginData.version ? req.body.pluginData.version : null
	};

	try {
		var updatedPlugin = await PluginService.updatePlugin(plugin);
		return res.status(200).json({status: 200, data: updatedPlugin});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.deletePlugin = async function(req, res, next) {
	if(!req.params.id)
		return res.status(400).json({status: 400, message: "Id must be present"});

	var id = req.params.id;

	try {
		var deletedPlugin = await PluginService.deletePlugin(id);
		return res.status(204).json({status:204, message: "Plugin deleted"});
	} catch(e) {
        return res.status(400).json({status: 400, message: e.message})
	}
};