'use strict';
const mongoose = require('mongoose');
const BoardService = require('../services/boards.service');
const sha1 = require('sha1');

const _this = this;

exports.getBoards = async function(req, res, next) {
	try {
		var boards = await BoardService.getBoards(req.body.query);
		return res.status(200).json({status: 200, data: boards});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.connectedBoards = async function(req, res, next) {
	return res.status(200).json({status: 200, data: req.app.locals.boards});
}

exports.getBoardById = async function(req, res, next) {
	if(!req.params.id)
		return res.status(400).json({status: 400, message: "Id must be present"});

	var id = mongoose.mongo.ObjectId(req.params.id);

	try {
		var board = await BoardService.getBoardById(id);
		return res.status(200).json({status: 200, data: board});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.createBoard = async function(req, res, next) {
	var board = {
		name: req.body.boardData.name,
		description: req.body.boardData.description,
		plugin: req.body.boardData.plugin,
		active: req.body.boardData.active
	};

	try {
		var newBoard = await BoardService.createBoard(board);
		return res.status(200).json({status: 200, data: board});
	} catch(e) {
		return res.status(400).json({status: 400, message: "Unable to create Board"});
	}
};

exports.updateBoard = async function(req, res, next) {
	if(!req.body.boardData._id)
		return res.status(400).json({status: 400, message: "Id must be present"});

	var id = req.body.boardData._id;

	var board = {
		id,
		name: req.body.boardData.name ? req.body.boardData.name : null,
		description: req.body.boardData.description ? req.body.boardData.description : null,
		plugin: req.body.boardData.plugin ? req.body.boardData.plugin : null,
		active: req.body.boardData.active ? req.body.boardData.active : false
	};

	try {
		var updatedBoard = await BoardService.updateBoard(board);
		return res.status(200).json({status: 200, data: updatedBoard});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.deleteBoard = async function(req, res, next) {
	if(!req.params.id)
		return res.status(400).json({status: 400, message: "Id must be present"});

	var id = req.params.id;

	try {
		var deletedBoard = await BoardService.deleteBoard(id);
		return res.status(204).json({status:204, message: "Board deleted"});
	} catch(e) {
        return res.status(400).json({status: 400, message: e.message})
	}
};