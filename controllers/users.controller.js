'use strict';
const mongoose = require('mongoose');
const UserService = require('../services/users.service');
const sha1 = require('sha1');

const _this = this;

exports.getUsers = async function(req, res, next) {
	try {
		var users = await UserService.getUsers(req.body.query);
		return res.status(200).json({status: 200, data: users});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.getUserById = async function(req, res, next) {
	if(!req.params.id)
		return res.status(400).json({status: 400, message: "Id must be present"});

	var id = mongoose.mongo.ObjectId(req.params.id);

	try {
		var user = await UserService.getUserById(id);
		return res.status(200).json({status: 200, data: user});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.createUser = async function(req, res, next) {
	var user = {
		username: req.body.userData.username,
		password: sha1(req.body.userData.password)
	};

	try {
		var newUser = await UserService.createUser(user);
		return res.status(200).json({status: 200, data: user});
	} catch(e) {
		return res.status(400).json({status: 400, message: "Unable to create User"});
	}
};

exports.updateUser = async function(req, res, next) {
	if(!req.body.userData._id)
		return res.status(400).json({status: 400, message: "Id must be present"});

	var id = req.body.userData._id;

	var user = {
		id,
		username: req.body.userData.username ? req.body.userData.username : null,
		password: req.body.userData.password ? sha1(req.body.userData.password) : null
	};

	try {
		var updatedUser = await UserService.updateUser(user);
		return res.status(200).json({status: 200, data: updatedUser});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.deleteUser = async function(req, res, next) {
	if(!req.params.id)
		return res.status(400).json({status: 400, message: "Id must be present"});

	var id = mongoose.mongo.ObjectId(req.params.id);

	try {
		var deletedUser = await UserService.deleteUser(id);
		return res.status(204).json({status:204, message: "User deleted"});
	} catch(e) {
        return res.status(400).json({status: 400, message: e.message})
	}
};