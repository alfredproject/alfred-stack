'use strict';
const glob = require('glob');
const path = require('path');
const express = require('express');
const PluginService = require('../services/plugins.service');

const _this = this;

var extensions = [];

exports.loadExtensions = async function() {
	var exts = glob.sync('./extensions/**/*.js');
	for (var i = 0; i < exts.length; i++) {
		let extension = require(path.resolve(exts[i]))
		await syncWithDB(extension);
		// extension = setRoutes(extension);
		extensions.push(extension);
	}
	return extensions;
};

var setRoutes = function(extension) {
	let router = express.Router();

	extension.routes.forEach(function(route) {
		if (route.type === 'get')
			router.get('/' + route.path, route.function);
		else if (route.type === 'post')
			router.post('/' + route.path, route.function);
		else if (route.type === 'put')
			router.put('/' + route.path, route.function);
		else if (route.type === 'delete')
			router.put('/' + route.path, route.function);
	});
	
	extension.router = router;
	return extension;
};

var syncWithDB = async function(extension) {
	var query = await PluginService.getPlugins({'name': extension.name});
	if (query.length == 0) {
		PluginService.createPlugin({'name': extension.name, 'author': extension.author, 'version': extension.version}).then(function(plugin) {
			extension._id = plugin._id;
		});
	} else if (query.length == 1) {
		extension._id = query[0]._id;
	}
	return;
}

exports.extensions = extensions;
