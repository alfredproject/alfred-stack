const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const UserService = require('../services/users.service');
const sha1 = require('sha1');

verifyToken = async function(token) {
	try {
		var decodedToken = jwt.verify(token, 'mysecretpassphrase');
		return decodedToken;
	} catch(e) {
		throw e;
	}
};

createToken = function(user) {
	if (!user)
		throw new Error('Must be passed a user');

	var token = jwt.sign({
		data: user
	}, 'mysecretpassphrase', {
		expiresIn: "2h",
		algorithm: 'HS256'
	});

	return token;
};

exports.login = async function(req, res, next) {
	try {
		var userQuery = await UserService.getUsers({'username': req.body.userData.username});
		if (userQuery.length < 1)
			return res.status(400).json({status: 400, message: 'Could not query users'});	

		var user = userQuery[0];
		if (user.password != sha1(req.body.userData.password))
			return res.status(400).json({status: 400, message: 'Invalid user'});

		var token = createToken(user);
		return res.status(200).json({status: 200, data: token});
	} catch(e) {
		return res.status(400).json({status: 400, message: e.message});
	}
};

exports.authorize = async function(req, res, next) {
	if (req.method == 'OPTIONS')
		next();
	var token = req.headers.authorization;
	try {
		var decoded = await verifyToken(token);
		next();
	} catch(e) {
		return res.status(403).json({status: 403, message: 'Invalid auth token provided'});	
	}
}