'use strict';
const express = require('express');
const router = express.Router();
const PluginController = require('../../controllers/plugins.controller');

router.get('/', PluginController.getPlugins);
router.get('/:id', PluginController.getPluginById);
router.post('/', PluginController.createPlugin);
router.put('/', PluginController.updatePlugin);
router.delete('/:id', PluginController.deletePlugin);

module.exports = router;