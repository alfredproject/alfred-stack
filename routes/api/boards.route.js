'use strict';
const express = require('express');
const router = express.Router();
const BoardController = require('../../controllers/boards.controller');
const BoardExtensionRoutes = require('./board-extensions.route.js');

router.get('/', BoardController.getBoards);
router.get('/connected', BoardController.connectedBoards);
// router.get('/:id', BoardController.getBoardById);
router.use('/:id', BoardExtensionRoutes);
router.post('/', BoardController.createBoard);
router.put('/', BoardController.updateBoard);
router.delete('/:id', BoardController.deleteBoard);

async function boardExtenionsApi(req, res, next) {
	var action = req.params.action;
	var id = req.params.id;
	console.log(id, action)
}

module.exports = router;