'use strict';
const express = require('express');
const router = express.Router({mergeParams: true});
const BoardController = require('../../controllers/boards.controller');
const BoardService = require('../../services/boards.service');
const ExtensionLoader = require('../../controllers/extension-loader.controller');
const PluginService = require('../../services/plugins.service');
const BoardManager = require('../../bin/board-manager');

router.get('/', BoardController.getBoardById);
router.delete('/', BoardController.deleteBoard);

router.get('/:action', boardExtenionsApi);
router.post('/:action', boardExtenionsApi);
router.put('/:action', boardExtenionsApi);
router.delete('/:action', boardExtenionsApi);


async function boardExtenionsApi(req, res, next) {
	var method = req.method;
	var action = req.params.action;

	var id = req.params.id;
	var board = await BoardService.getBoardById(id);
	var plugin = ExtensionLoader.extensions.find(function(ext) { return ext._id.toString() == board.plugin.toString(); });

	if (plugin.routes.find(function(route) { return route.path == action})) {
		try {
			var command = plugin.routes.find(function(route) { return route.path == action && route.type.toUpperCase() == method}).command;
			var socket = BoardManager.boards.find(b => b.name == board.name).socket;
			BoardManager.sendToBoard(req.app.locals.tcp, {'socket': socket, 'action': command})
			return res.status(200).json({status: 200, data: 'Success'});
			// return plugin.routes.find(function(route) { return route.path == action && route.type.toUpperCase() == method}).function(req, res, next);
		} catch(e) {
			console.log(e);
		}
	}

	return res.status(400).json({status: 404, message: "Not found"});
}

// while(ExtensionLoader.extensions.length == 0) {}
// ExtensionLoader.extensions.forEach(function(extension) {
// 	while(extension._id == undefined) {}

// });
module.exports = router;