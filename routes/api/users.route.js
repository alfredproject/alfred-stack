'use strict';
const express = require('express');
const router = express.Router();
const UserController = require('../../controllers/users.controller');

router.get('/', UserController.getUsers);
router.get('/:id', UserController.getUserById);
router.post('/', UserController.createUser);
router.put('/', UserController.updateUser);
router.delete('/:id', UserController.deleteUser);

module.exports = router;