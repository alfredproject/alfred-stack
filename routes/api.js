const express = require('express');
const router = express.Router();

const users = require('./api/users.route');
const plugins = require('./api/plugins.route');
const boards = require('./api/boards.route');
const AuthController = require('../controllers/auth.controller');

router.post('/login', AuthController.login);

router.all('*', AuthController.authorize);

router.use('/users', users);
router.use('/plugins', plugins);
router.use('/boards', boards);

module.exports = router;
