'use strict';

const Logs = [];

var webSocket = {};

var sendMessage = function(message) {
	webSocket.clients.forEach(client => {
		client.send(JSON.stringify(message));
	});
};

var sendCompleteLog = function(ws) {
	Logs.forEach(log => {
		ws.send(JSON.stringify(log));
	});
};

exports.addLog = function(message)  {
	let msg = {'content': message, 'date': new Date()};
	Logs.push(msg);
	sendMessage(msg);
};

exports.setWebSocketInterface = function(wss) {
	webSocket = wss;

	webSocket.on('connection', (ws) => {
		ws.isAlive = true;
		// Check for ping response
		ws.on('pong', () => {
	        ws.isAlive = true;
	    });

		sendCompleteLog(ws);
	});
	// Check if the connection is still open every 10 seconds
	setInterval(() => {
		webSocket.clients.forEach((ws) => {
			if (!ws.isAlive) return ws.terminate();

			ws.isAlive = false;
			ws.ping(null, false, true);
		});
	}, 10000);
};

exports.Logs = Logs;