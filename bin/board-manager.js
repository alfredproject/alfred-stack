'use strict';
const BoardService = require('../services/boards.service');
const Logger = require('./logger');

var boards = [];

exports.addConnectedBoard = async function(data) {
	data.name = data.name.replace("\n", "");
	var query = await BoardService.getBoards({'name': data.name});
	if (query.length >= 1) {
		boards.push({'name': data.name, 'socket': data.socket, 'startTime': Date()});
		Logger.addLog(data.name + " on socket: " + data.socket + " has connected.");
	}
};

exports.removeDisconnectedBoard = function(socket) {
	let idx = boards.indexOf( boards.find(b => b.socket === socket));
	if (idx != -1) {
		Logger.addLog(boards[idx].name + " on socket: " + boards[idx].socket + " has disconnected.");
		boards.splice(idx, 1);
	}
}

exports.sendToBoard = function(tcp, data) {
	var board = boards.find(b => b.socket === data.socket)
	Logger.addLog(board.name + " on socket: " + board.socket + " has launched: '" + data.action + "'.");
	tcp.to.emit("execute", JSON.stringify({socket: data.socket, action: data.action}));
}

exports.boards = boards;