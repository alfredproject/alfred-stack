'use strict';
var Test = function() {};
Test.prototype.name = 'extension-test';
Test.prototype.author = 'Juan Manez';
Test.prototype.version = '1.0.0';
Test.prototype.routes = [
	{
		path: 'test',
		type: 'get',
		// function: function(req, res, next) { return res.status(200).json({status: 200, data: 'test success'}); }
		command: 'ledon'
	}
];
module.exports = new Test();